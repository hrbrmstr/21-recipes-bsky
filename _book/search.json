[
  {
    "objectID": "index.html",
    "href": "index.html",
    "title": "21 Recipes For Mining Bluesky Data with {atproto}",
    "section": "",
    "text": "Preface\nThis is a book similar to my 2018 “21 Recipes For Mining Twitter Data with {rtweet}”. Both it and this tome are based on Matthew R. Russell’s book. That book is out of distribution and much of the content is in Matthew’s “Mining the Social Web” book. There will be many similarities between his “21 Recipes” book and this book on purpose. I am not claiming originality in this work, just making an R-centric version of the cookbook.\nWell, that’s a (how shall we describe it?) a “misdirection”. While we will most certainly be using R for the examples, we’ll be using using the atproto Python module by Ilya Siamionau via R’s {reticulate} package. We’re doing this because the AT Protocol is super new, has some complexity the Twitter API does not, and Ilya has done an fantastic job designing the Python code, and is keeping up with API changes. This means that there may be many tweaks to this book as the AT Protocol settles down."
  },
  {
    "objectID": "index.html#what-youll-need",
    "href": "index.html#what-youll-need",
    "title": "21 Recipes For Mining Bluesky Data with {atproto}",
    "section": "What You’ll Need",
    "text": "What You’ll Need\nWell, you will need both R and Python installed. To keep this short, we’ll assume you know how to get both on your system by following the myriad of possible directions. However, I will add that I highly suggest reading “Relieving your Python packaging pain” by Bite Code if you value your sanity. Despite the name, it’s not about writing Python packages but how to not turn into The Incredible Hulk whilst using Python.\nTo wire-up the {atproto} Python package, run the following in your R console:\n\ninstall.packages(\"reticulate\")\nreticulate::py_install(\"atproto\")\n\nI do not normally do that dance since it highly favors the use of Anaconda, and I am not a huge fan of that ecosystem. But, it will follow the best practices in that “Releiving” post, and should help your wiring up R and Python a bit less painful.\nTo test the setup, you can do:\n\nlibrary(reticulate)\n\n(atproto &lt;- import(\"atproto\"))\n\nModule(atproto)\n\n\nIf you don’t see Module(atproto) drop an issue on this repo and I’ll try to help as best as possible.\nThese are the versions of R and Python that were used to execute all the examples contained within this tome:\n\nR.version.string\n\n[1] \"R version 4.3.1 Patched (2023-06-16 r84562)\"\n\npy_version()\n\n[1] '3.11'"
  },
  {
    "objectID": "index.html#conventions",
    "href": "index.html#conventions",
    "title": "21 Recipes For Mining Bluesky Data with {atproto}",
    "section": "Conventions",
    "text": "Conventions\nCode blocks will appear like they do, above. I will be using other R packages, and leave the install.packages(…) dance to you if you encounter any that aren’t already in your R setup. If I ever use “development mode” packages, I’ll add a comment after the library(…) call for them. They’ll genereally be installed from R Universe, but I may include some direct-to-repo install commands as well."
  },
  {
    "objectID": "about.html#contributors",
    "href": "about.html#contributors",
    "title": "About the Authors",
    "section": "Contributors",
    "text": "Contributors\nThis is an open source effort and the goal is to have all assistance acknowledged as it is very, very, very much appreciated. The following folks have gone out of their way to make this a better resource. They. Are. Awesome.\n(Hint: fixes/additions are welcome and encouraged and you will 100% get full credit for them!)"
  },
  {
    "objectID": "01-recipe.html#problem",
    "href": "01-recipe.html#problem",
    "title": "1  Using Application Passwords To Access Bluesky",
    "section": "1.1 Problem",
    "text": "1.1 Problem\nYou want to access your own data or another user’s data for analysis."
  },
  {
    "objectID": "01-recipe.html#solution",
    "href": "01-recipe.html#solution",
    "title": "1  Using Application Passwords To Access Bluesky",
    "section": "1.2 Solution",
    "text": "1.2 Solution\nTake advantage of Bluesky’s / the AT Protocol’s app passwords to gain full access to Bluesky’s entire API."
  },
  {
    "objectID": "01-recipe.html#discussion",
    "href": "01-recipe.html#discussion",
    "title": "1  Using Application Passwords To Access Bluesky",
    "section": "1.3 Discussion",
    "text": "1.3 Discussion\nYou will need a Bluesky account to use almost all the code in this book. One exception is access to the firehose (which we’ll get into later).\nUnlike Twitter, and other sites, which make you go through an OAuth dance, Bluesky / the AT Protocol relies on App Passwords. These app passwords are both simpler than OAuth, and prevent apps from abusing your most sensitive settings.\nOn Bluesky, you create said beasts here: https://bsky.app/settings/app-passwords. The rest of this book relies on you storing your Bluesky handle (e.g. handle.bsky.social or a domain you’ve claimed on it, like mine, hrbrmstr.dev) in an environment variable BSKY_USER and the generated application password in BSKY_PASS. In R, the best way to ensure those are available to your R sessions is by putting them in ~/.Renviron or equivalent.\nBefore logging in to Bluesky programmatically, we need to import the atproto package and create a client:\n\nlibrary(reticulate)\n\natproto &lt;- import(\"atproto\")\n\nclient &lt;- atproto$Client()\n\nprofile &lt;- client$login(Sys.getenv(\"BSKY_USER\"), Sys.getenv(\"BSKY_PASS\"))\n\nnames(profile)\n\n [1] \"avatar\"         \"banner\"         \"description\"    \"did\"           \n [5] \"displayName\"    \"followersCount\" \"followsCount\"   \"handle\"        \n [9] \"indexedAt\"      \"labels\"         \"postsCount\"     \"viewer\"        \n\n\nAfter the login is successful, profile has a number of slots that contain information about the user.\n\nnames(profile) |&gt; \n  sapply(\\(.x) profile[[.x]]) |&gt; \n  str()\n\nList of 12\n $ avatar        : chr \"https://cdn.bsky.social/imgproxy/XP3hrl0QyMpTYe9MnmXfUCEUol_27qqRGOloaJPYxmQ/rs:fill:1000:1000:1:0/plain/bafkre\"| __truncated__\n $ banner        : chr \"https://cdn.bsky.social/imgproxy/0OAqfyczYKU1ATNSPWbFkwM3J6CEYTCDTJZHxGlR2GI/rs:fill:3000:1000:1:0/plain/bafkre\"| __truncated__\n $ description   : chr \"a.k.a. boB Rudis • 🇺🇦 Pampa • Don't look at me…I do what he does—just slower. #rstats #javascript #datascience \"| __truncated__\n $ did           : chr \"did:plc:hgyzg2hn6zxpqokmp5c2xrdo\"\n $ displayName   : chr \"hrbrmstr\"\n $ followersCount: int 60\n $ followsCount  : int 70\n $ handle        : chr \"hrbrmstr.dev\"\n $ indexedAt     : chr \"2023-07-05T20:13:23.951Z\"\n $ labels        : list()\n $ postsCount    : int 50\n $ viewer        :ViewerState(blockedBy=False, blocking=None, followedBy=None, following=None, muted=False, mutedByList=None, _type='app.bsky.actor.defs#viewerState')\n\n\nThe did, or decentralized identifier (DID) is super important. This particular DID is specific to Bluesky and is referred to as a placeholder DID as the Bluesky developers do not want it to stick around forever in its current form. They are actively hoping to replace it with or evolve it into something less centralized - likely a permissioned DID consortium. They will, however, suppoort did:plc in the current form until after any successor is deployed, with a reasonable grace period. They’ll also provide a migration route to allow continued use of existing did:plc identifiers.\nYou can use this did:plc in place of your handle in BSKY_USER if you’d prefer.\nThe use of client$login(…) creates and stores a session in your running environment, which will let you access more client methods without the need to reauthenticate each time. These are the top-level methods:\n\nsort(names(client))\n\n [1] \"bsky\"             \"com\"              \"delete_post\"      \"invoke_procedure\"\n [5] \"invoke_query\"     \"like\"             \"login\"            \"me\"              \n [9] \"repost\"           \"request\"          \"send_image\"       \"send_post\"       \n[13] \"unlike\"          \n\n\n\nbsky provides access to lower-level methods in the bsky namespace which include:\n\nactor\nfeed\ngraph\nnotification\nunspecced\n\ncom does the same for the com namespace, which (for now) has one next-level domain: atproto with the following methods:\n\nadmin\nidentity\nlabel\nmoderation\nrepo\nserver\nsync\n\n\nThe rest of the client methods are pretty self-explanatory."
  },
  {
    "objectID": "01-recipe.html#see-also",
    "href": "01-recipe.html#see-also",
    "title": "1  Using Application Passwords To Access Bluesky",
    "section": "1.4 See Also",
    "text": "1.4 See Also\n\nIntroduction - The AT Protocol SDK."
  },
  {
    "objectID": "02-trending-topics.html#problem",
    "href": "02-trending-topics.html#problem",
    "title": "2  Looking Up the Trending Topics",
    "section": "2.1 Problem",
    "text": "2.1 Problem\nYou want to keep track of the trending/“hot” topics on Bluesky over a period of time."
  },
  {
    "objectID": "02-trending-topics.html#solution",
    "href": "02-trending-topics.html#solution",
    "title": "2  Looking Up the Trending Topics",
    "section": "2.2 Solution",
    "text": "2.2 Solution\nUse methods in app.bsky.feed with the “What’s Hot” Bluesky-network-wide feed."
  },
  {
    "objectID": "02-trending-topics.html#discussion",
    "href": "02-trending-topics.html#discussion",
    "title": "2  Looking Up the Trending Topics",
    "section": "2.3 Discussion",
    "text": "2.3 Discussion\nFor the moment, Bluesky purports to believe you own your own attention. This means you choose how you engage with content on Bluesky, and can use the built-in algorithmic feeds provided by Bluesky, pick from published algorithmic feeds created by folks you trust, or build your own.\nThe URL for the “What’s Hot” feed gives us some information we can use to access it programmatically:\nhttps://bsky.app/profile/did:plc:z72i7hdynmk6r22z27h6tvur/feed/whats-hot\n\ndid:plc:z72i7hdynmk6r22z27h6tvur is the actor\nwhats-hot is the short name of the feed\n\nWe’ll need a full at:// URI to be able to get access to it, so let’s look for it using the API:\n\nlibrary(reticulate)\n\natproto &lt;- import(\"atproto\")\n\nclient &lt;- atproto$Client()\n\nprofile &lt;- client$login(Sys.getenv(\"BSKY_USER\"), Sys.getenv(\"BSKY_PASS\"))\n\nbsky_feeds &lt;- client$bsky$feed$get_actor_feeds(list(actor = \"did:plc:z72i7hdynmk6r22z27h6tvur\"))\n\nsort(names(bsky_feeds$feeds[[1]]))\n\n [1] \"avatar\"            \"cid\"               \"creator\"          \n [4] \"description\"       \"descriptionFacets\" \"did\"              \n [7] \"displayName\"       \"indexedAt\"         \"likeCount\"        \n[10] \"uri\"               \"viewer\"           \n\ndata.frame(\n  display_name = sapply(bsky_feeds$feeds, \\(.x) .x$displayName),\n  uri = sapply(bsky_feeds$feeds, \\(.x) .x$uri)\n)\n\n          display_name\n1              Mutuals\n2      Best of Follows\n3 Popular With Friends\n4         Bluesky Team\n5   What's Hot Classic\n6           What's Hot\n                                                                            uri\n1         at://did:plc:z72i7hdynmk6r22z27h6tvur/app.bsky.feed.generator/mutuals\n2 at://did:plc:z72i7hdynmk6r22z27h6tvur/app.bsky.feed.generator/best-of-follows\n3    at://did:plc:z72i7hdynmk6r22z27h6tvur/app.bsky.feed.generator/with-friends\n4       at://did:plc:z72i7hdynmk6r22z27h6tvur/app.bsky.feed.generator/bsky-team\n5     at://did:plc:z72i7hdynmk6r22z27h6tvur/app.bsky.feed.generator/hot-classic\n6       at://did:plc:z72i7hdynmk6r22z27h6tvur/app.bsky.feed.generator/whats-hot\n\n\nWe can use the “What’s Hot” URI to get a list of the items in that feed:\n\nclient$bsky$feed$get_feed(\n  list(\n    feed = \"at://did:plc:z72i7hdynmk6r22z27h6tvur/app.bsky.feed.generator/whats-hot\"\n  )\n) -&gt; hot_feed\n\nI’ve been glossing over the cursor slot in most of the return values since this is just a cookbook to get folks started. For any method that supports cursor-based pagination, you can add it as a parameter to the list. For example, this:\n\nclient$bsky$feed$get_feed(\n  list(\n    feed = \"at://did:plc:z72i7hdynmk6r22z27h6tvur/app.bsky.feed.generator/whats-hot\",\n    cursor = hot_feed$cursor\n  )\n)\n\nwill get the next 50 (the current default page size) items in the feed.\nInside hot_feed has a feed slot which is a list of posts. Each post is a large, nested structure which we’ll look at via the Python object pretty-printed view of one example record:\nFeedViewPost(\n  post=PostView(\n    author=ProfileViewBasic(\n      did='did:plc:xydmw4rhlnb4mrz5eyw3z6ak',\n      handle='yesitscolin.bsky.social',\n      avatar='https://cdn.bsky.social/imgproxy/_XZvSpVnwSOoZYun4CrxOERXmtD0FgOTz9ffXqbUnmE/rs:fill:1000:1000:1:0/plain/bafkreidluvoz62jftcf6iqsjjo7x46s237jbr6kkluecsallz6oci2k33q@jpeg',\n      displayName='Colin',\n      labels=[],\n      viewer=ViewerState(\n        blockedBy=False,\n        blocking=None,\n        followedBy=None,\n        following=None,\n        muted=False,\n        mutedByList=None,\n        _type='app.bsky.actor.defs#viewerState'\n      ),\n      _type='app.bsky.actor.defs#profileViewBasic')\n    ,\n    cid='bafyreigrcsumojr4k2cy7alnloeyxm464ab4exdt2orcfluvjzpe5t5uu4',\n    indexedAt='2023-07-08T13:16:24.879Z',\n    record=Main(\n      createdAt='2023-07-08T13:16:24.760Z',\n      text='Shoutout to everyone who still cuts these up before throwing them away because they told us all when we were kids that fish get stuck in them and die…',\n      embed=Main(\n        images=[\n          Image(\n            alt='A hand holding a plastic rings drink container',\n            image=&lt;atproto.xrpc_client.models.blob_ref.BlobRef object at 0x168b58890&gt;,\n            _type='app.bsky.embed.images#image'\n        )],\n        _type='app.bsky.embed.images'),\n      entities=None,\n      facets=None,\n      langs=['en'],\n      reply=None,\n      _type='app.bsky.feed.post'\n    ),\n    uri='at://did:plc:xydmw4rhlnb4mrz5eyw3z6ak/app.bsky.feed.post/3jzzabaprgx26',\n    embed=View(\n      images=[ViewImage(\n        alt='A hand holding a plastic rings drink container',\n        fullsize='https://cdn.bsky.social/imgproxy/LWNCHRKfUz-hYAc5f9xwtL7f7BhzMgtEVdXX8c6EBIY/rs:fit:2000:2000:1:0/plain/bafkreihrnrzbyypa3cmsk2bgruci7hsssjnv3l7dlv5x257utpzg7twcim@jpeg',\n        thumb='https://cdn.bsky.social/imgproxy/BAD839Z-NtgH_PHGUbkaJOlq2E8KeXJbPfCC8xDVmNY/rs:fit:1000:1000:1:0/plain/bafkreihrnrzbyypa3cmsk2bgruci7hsssjnv3l7dlv5x257utpzg7twcim@jpeg',\n        _type='app.bsky.embed.images#viewImage'\n      )],\n      _type='app.bsky.embed.images#view'\n    ),\n    labels=[],\n    likeCount=555,\n    replyCount=76,\n    repostCount=42,\n    viewer=ViewerState(\n      like=None,\n      repost=None,\n      _type='app.bsky.feed.defs#viewerState'\n    ),\n    _type='app.bsky.feed.defs#postView'),\n  reason=None,\n  reply=None,\n  _type='app.bsky.feed.defs#feedViewPost'\n)\nYou can pick and choose what components from each nested structure you want and then do something similar to what I suggested in the Twitter recipes to save the data locally."
  },
  {
    "objectID": "02-trending-topics.html#see-also",
    "href": "02-trending-topics.html#see-also",
    "title": "2  Looking Up the Trending Topics",
    "section": "2.4 See Also",
    "text": "2.4 See Also\n\napp.bsky.feed | AT Protocol"
  }
]